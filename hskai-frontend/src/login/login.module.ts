import {NgModule} from '@angular/core';
import {LoginComponent} from './login.component';

@NgModule({
  declarations: [LoginComponent],
  imports: [],
  exports: [],
  providers: []
})

export class LoginModule {
}
