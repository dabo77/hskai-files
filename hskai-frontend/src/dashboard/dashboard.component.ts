import {Component} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.scss']
})

export class DashboardComponent {
  constructor(private router: Router) {
  }

  public navigate(): void {
    this.router.navigateByUrl('/login');
  }
}
