package edu.hska.hskaifiles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HskaiFilesApplication {

	public static void main(String[] args) {
		SpringApplication.run(HskaiFilesApplication.class, args);
	}

}
